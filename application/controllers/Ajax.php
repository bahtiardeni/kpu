<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Global_model $global_model
 * @property User_model $user_model
 *
 */

class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("user_model");
    }

    public function do_login()
    {
        $username	= $this->input->post("username");
        $password	= $this->input->post("password");

        $result		= $this->access->login($username, $password);

        echo json_encode($result);
    }

    function datatable()
    {
        $type			= $this->input->get("type");
        $page			= $this->input->get("page");
        $size			= (int)$this->input->get("size");
        $sort			= $this->input->get("sort");
        $search			= $this->input->get("search");
        $paged			= $page	== 1 ? 0 : (($page - 1) * $size);
        $link_uptede	= $this->input->get("link_update");

        $condition = array();

        if (!empty($search))
        {
            foreach($search as $field => $value)
            {
                //var_dump($field);
                $value = trim($value);

                if(strlen($value) && $value != "-all-")
                {
                    if ($type == "satker" && $field == "eselon_id")
                    {
                        $condition["mst_satker.".$field] = $value;
                    }
                    else
                    {
                        $condition[$field] = $value;
                    }
                }
            }
        }

        $initial_table	= $this->initial_datatable($type);

        if ($type == "user")
        {
            $data	= $this->user_model->get_data($condition, $initial_table["table_name"].".created", "DESC", $paged, $size)->result();
            $count 	= $this->user_model->get_data($condition)->num_rows();
        }
        else
        {
            $data	= $this->global_model->get_data($initial_table["table_name"], $condition, $initial_table["table_name"].".".$initial_table["sort_field"], $initial_table["sort_type"], $paged, $size)->result();

            $count 	= $this->global_model->get_data($initial_table["table_name"], $condition)->num_rows();
        }

        $row_data		= array();

        foreach($data as $item_data)
        {
            $row = array();

            if ($type == "realisasi")
            {
                $row	= array(
                    "realisasi_id"		=> $this->hashids->encode($item_data->realisasi_id),
                    "ta"				=> $item_data->ta,
                    "kodeakun"			=> $item_data->kodeakun,
                    "namaakun"			=> $item_data->namaakun,
                    "target"			=> "<span class='realisasi-target hidden'>".$item_data->target."</span>".number_format($item_data->target)
                );

                $months	= array("januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","nopember","desember");

                $total = 0;
                foreach ($months as $key => $month)
                {
                    $disabled = "";

                    if ($key + 1 > $curent_month){
                        $disabled	= "disabled";

                    }

                    $content	= "
						<form method='post' id='realisasi-".$this->hashids->encode($item_data->realisasi_id)."'>
							<input type='hidden' name='realisasi_id' value='".$this->hashids->encode($item_data->realisasi_id)."'> 
							<input type='text' ".$disabled." class='form-control input-sm number textbox-realisasi' name='data[".$month."]' value='".(!empty($item_data->$month) ? $item_data->$month : 0)."' style='padding: 3px; height: 20px; width: 120px;'>
						</form>
					";

                    $row[$month]	= $content;
                    $total = $total + (!empty($item_data->$month) ? $item_data->$month : 0);
                }
                $row["total"]	= "<span class='realisasi-total'>".number_format($total)."</span>";

                $persen			= empty($item_data->target) ? 0 : round($total / $item_data->target * 100, 2);

                $row["persen"]	= "<span class='realisasi-persen'>".$persen."</span>";
            }
            else
            {
                foreach($initial_table["field_table"] as $field)
                {
                    if($field == "status" && $type != "log_login")
                    {
                        $row[$field] = $item_data->$field == 1 ? "Aktif" : "Tidak Aktif";
                    }
                    else
                    {
                        $row[$field] = $item_data->$field;
                    }

                    if(in_array($field, $initial_table["number_format"]))
                    {
                        $row[$field] = number_format($row[$field]);
                    }

                    if(in_array($field, $initial_table["class_center"]))
                    {
                        $row[$field] = "<div class='text-center'>".$row[$field]."</div>";
                    }
                    else if(in_array($field, $initial_table["class_right"]))
                    {
                        $row[$field] = "<div class='text-right'>".$row[$field]."</div>";
                    }

                }

                if ($type == "user"){
                    $row["action"] = "
                        <div style='margin: -5px;'>
                        <a href='".base_url("user/update/".$this->hashids->encode($item_data->$initial_table["primary_key"]))."'><span class='fa fa-pencil-square fa-2x'></span></a> &nbsp; 
                        <a href='#'><span class='fa fa-trash fa-2x'></span></a>
                        </div>
					";

                    /*$row["action"] = "
                        <div class='btn-group text-center'>
                            <a href='".base_url("user/update/".$this->hashids->encode($item_data->$initial_table["primary_key"]))."' class='btn btn-default btn-xs' >
                                <span class='fa fa-pencil-square fa-2x'></span>
                            </a>
                            <a href='#' class='btn btn-default btn-xs'><span class='fa fa-trash fa-2x'></span></a>
                        </div>

					";*/

                    /*<div class='btn-group'>
							<button data-toggle='dropdown' class='btn btn-info btn-xs dropdown-toggle' aria-expanded='false'>Action <span class='caret'></span></button>
							<ul class='dropdown-menu dropdown-menu-right'>
								<li><a href='".base_url($link_uptede.$this->hashids->encode($item_data->$initial_table["primary_key"]))."'><span class='fa fa-pencil-square'></span> Edit</a></li>
								<li><a href='javascript:void(0)' data-type='".$type."' data-id='".$this->hashids->encode($item_data->$initial_table["primary_key"])."' class='btn-reset-password'><span class='fa fa-warning '></span> Reset Password</a></li>
								<li><a href='javascript:void(0)' data-type='".$type."' data-id='".$this->hashids->encode($item_data->$initial_table["primary_key"])."' class='btn-remove-data'><span class='fa fa-trash'></span> Delete</a></li>
							</ul>
						</div>*/

                }else if ($type != "log_login"){
                    if ($type != "audittrail")
                    {

                        $row["action"] = "
							<div class='btn-group'>
								<button data-toggle='dropdown' class='btn btn-info btn-xs dropdown-toggle' aria-expanded='false'>Action <span class='caret'></span></button>
								<ul class='dropdown-menu dropdown-menu-right'>
									<li><a href='".base_url($link_uptede.$this->hashids->encode($item_data->$initial_table["primary_key"]))."'><span class='fa fa-pencil-square'></span> Edit</a></li>
									<li><a href='javascript:void(0)' data-type='".$type."' data-id='".$this->hashids->encode($item_data->$initial_table["primary_key"])."' class='btn-remove-data'><span class='fa fa-trash'></span> Delete</a></li>
								</ul>
							</div>
						";
                    }
                }
            }



            $row_data[] = $row;
        }

        $json_data = array(
            "datatable"     => $row_data,
            "count"         => $count,
            "dataStart"     => $paged + 1,
            "dataSize"      => $size,
            "field_mobile"  => $initial_table["field_mobile"]
        );

        header('Content-Type: application/json');
        echo json_encode($json_data);
    }

    function initial_datatable($type)
    {
        $return	= array();
        $return["number_format"]	= array();

        if ($type == "user")
        {
            $return	= array(
                "table_name"	=> "mst_user",
                "primary_key"	=> "user_id",
                "field"			=> array("user_id","username","name","email","password","email","hp","status","created","updated","role_id","role_name"),
                "field_table"	=> array("role_name","name","username","email","hp","created","updated"),
                "class_center"	=> array("created","updated","status","hp","action"),
                "class_right"	=> array(),
                "number_format"	=> array(),
                "sort_field"	=> "created",
                "sort_type"		=> "DESC",

                "field_required" => array(
                    "role_id" 	=> "Role",
                    "username" 	=> "Username",
                    "name" 	    => "Nama Lengkap",
                    "password" 	=> "Password",
                    "email" 	=> "Email",
                    "status" 	=> "Status",
                ),

                "field_mobile" => array("role_name", "name", "action")
            );
        }



        return $return;
    }

    function datatable_update()
    {
        $type	= $this->input->post("type");
        $input	= $this->input->post("input");

        $initial_table	= $this->initial_datatable($type);

        $table_name		= $initial_table["table_name"];
        $primary_key	= $initial_table["primary_key"];
        $field_required	= $initial_table["field_required"];
        $table_field	= $initial_table["field"];

        $result			= "error";
        $message		= "";

        $data_new		= array();

        foreach ($input as $field => $value)
        {
            $value	= trim($value);

            if (array_key_exists($field, $field_required) && ($value == '' && $value !== 0 && $value !== '0'))
            {
                $message	.= $field_required[$field]." Tidak boleh kosong.<br>";
            }

            if ($type == "user")
            {
                $id	= $this->hashids->decode($input[$primary_key]);
                $id	= !empty($id[0]) ? $id[0] : null;

                if ($field == "username")
                {
                    // Cek Duplikasi
                    $condition			= array();
                    $condition[$field]	= $value;

                    if (!empty($id))
                        $condition[$primary_key." != "]	= $id;

                    $cek_data	= $this->global_model->get_data($table_name, $condition)->num_rows();

                    if (!empty($cek_data)){
                        $message	.= "Username <b>".$value."</b> telah ada.<br>";
                    }

                }
                else if ($field == "email")
                {
                    if (!valid_email($value))
                    {
                        $message	.= "Email <b>".$value."</b> tidak valid.<br>";
                    }
                    else
                    {
                        // Cek Duplikasi
                        $condition			= array();
                        $condition[$field]	= $value;

                        if (!empty($id))
                            $condition[$primary_key." != "]	= $id;

                        $cek_data	= $this->global_model->get_data($table_name, $condition)->num_rows();

                        if (!empty($cek_data)){
                            $message	.= "Email <b>".$value."</b> telah ada.<br>";
                        }
                    }
                }

            }

            if (in_array($field, $table_field))
            {
                if ($field == $primary_key)
                {
                    if (empty($value)){
                        $field_id	= "";
                    }else{
                        $field_id	= $this->hashids->decode($value);
                        $field_id	= $field_id[0];
                    }

                    $data_new[$field]	= $field_id;
                }
                else if ($field == "password")
                {
                    if ((!empty($input[$primary_key]) && $value != $input["old_password"]) || empty($input[$primary_key])){
                        $data_new[$field]	= sha1(md5(trim($value)));
                    }
                    else
                    {
                        $data_new[$field]	= $value;
                    }
                }
                else
                {
                    $data_new[$field]	= $value;
                }
            }
        }

        if ($type == "change_password")
        {
            $password1	= $input["password1"];
            $password2	= $input["password"];
            $password3	= $input["password3"];

            if (sha1(md5($password1)) != $this->_user->password)
            {
                $message	= "- Password lama tidak sesuai.<br>".$message;
            }

            if ($password2 != $password3)
            {
                $message	= "- Ulangi password baru dengan benar.<br>".$message;
            }

            if ($password1 == $password2)
            {
                $message	= "- Password baru tidak boleh sama dengan password lama.<br>".$message;
            }

            if (strlen($password2) < 8) {
                $message	= "- Password min 8 karakter.<br>".$message;
            }
            elseif(!preg_match("#[0-9]+#", $password2)) {
                $message	= "- Password harus mengandung huruf dan angka.<br>".$message;
            }
            elseif(!preg_match("#[A-Z]+#",$password2) && !preg_match("#[a-z]+#",$password2)) {
                $message	= "- Password harus mengandung huruf dan angka.<br>".$message;
            }
        }

        if (empty($message))
        {
            if (empty($data_new[$primary_key]))
            {
                $old_data				= array();

                $data_id = $this->global_model->save($table_name, $data_new);

            }
            else
            {
                $old_data				= (array)$this->global_model->get_data($table_name, array(
                    $primary_key => $data_new[$primary_key]
                ))->row();

                $this->global_model->update($table_name, array(
                    $primary_key => $data_new[$primary_key]
                ), $data_new);

                $data_id 	= $data_new[$primary_key];
            }

            $result		= "success";
            $message	= "Data berhasil disimpan.";

        }

        echo json_encode(array(
            "result"	=> $result,
            "message"	=> $message
        ));
    }
}
