<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Global_model $global_model
 *
 * @property Access $access
 *
 */

class Auth extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function login()
	{
		$this->access->is_login() ? redirect(base_url()) : null;

		$data["title"]		= "Login";
		$this->load->view("auth/login", $data);
	}
	
	function ajax_login()
	{
		$username	= $this->input->post("username");
		$password	= $this->input->post("password");
		$captcha	= $this->input->post("captcha");
		
		$result		= $this->access->login($username, $password, $captcha);
		
		echo json_encode($result);
	}
	
	function logout()
	{
		$user_id	= $this->session->userdata("user_id");

		$this->session->sess_destroy();
		redirect(base_url("auth/login"));
	}

}
