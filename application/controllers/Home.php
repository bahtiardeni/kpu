<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Global_model $global_model
 *
 * @property Access $access
 *
 */

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["title"]	= "Dashboard";
        $data["_user"]	= $this->_user;

        $this->template->generate_template("home/index", $data);
    }
}
