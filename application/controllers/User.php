<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Global_model $global_model
 *
 * @property Access $access
 * @property Formlib $formlib
 *
 */

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["title"]	= "Data User";
        $data["_user"]	= $this->_user;

        $data["roles"]          = $this->global_model->get_data("mst_role", array("id !=" => 1))->result();
        $data["link_update"]    = "user/update/";
        $data["datatable"]      = "user";

        $this->template->generate_template("user/index", $data);
    }

    function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        if (empty($id))
        {
            $title	= "Tambah Data User";

            $user_id 		= "";
            $username 		= "";
            $name    		= "";
            $password 		= "";
            $role_id 		= "";
            $email 			= "";
            $hp 			= "";
            $status      	= "";

        }
        else
        {
            $edit	= $this->global_model->get_data("mst_user", array(
                "user_id"	=> $id
            ))->row();

            $title			= "Ubah Data User";
            $user_id 		= $id_hash;
            $username 		= $edit->username;
            $name    		= $edit->name;
            $password 		= $edit->password;
            $role_id 		= $edit->role_id;
            $email 			= $edit->email;
            $hp 			= $edit->hp;
            $status 	    = $edit->status;
        }

        $data["type"]				    = $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "user", "hidden");

        $data["input"]["user_id"]  		= $this->formlib->_generate_input_text("user_id", "input[user_id]", "user_id", $user_id, "hidden");
        $data["input"]["username"]  	= $this->formlib->_generate_input_text("username", "input[username]", "Username", $username);
        $data["input"]["name"]  	    = $this->formlib->_generate_input_text("fullname", "input[name]", "Nama Lengkap", $name);
        $data["input"]["old_password"]  = $this->formlib->_generate_input_text("password", "input[old_password]", "Password", $password, "hidden");
        $data["input"]["password"]  	= $this->formlib->_generate_input_text("password", "input[password]", "Password", $password, "password");
        $data["input"]["role_id"]  		= $this->formlib->_generate_dropdown_table("mst_role", array(), "id", "role_name", "id", "input[role_id]", $role_id );
        $data["input"]["email"]  		= $this->formlib->_generate_input_text("email", "input[email]", "Email", $email);
        $data["input"]["hp"]  			= $this->formlib->_generate_input_text("hp", "input[hp]", "Nomor HP", $hp);
        $data["input"]["status"]        = $this->formlib->_generate_dropdown("status_aktif", "input[status]", array(
            "1" => "Aktif",
            "0" => "Tidak Aktif"
        ), $status);



        $data["title"]		= $title;
        $data["link_back"]	= base_url("user/");

        $data["_user"]	= $this->_user;
        $this->template->generate_template("user/update", $data);
    }
}
