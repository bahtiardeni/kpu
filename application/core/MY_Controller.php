<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public $_user;
	
	public function __construct()
    {
        parent::__construct();
        
        $this->load->model("global_model");
        
        $controller	= $this->router->fetch_class();
		$method		= $this->router->fetch_method();
		
        if (!empty($this->session->userdata("user_id")))
        {
        	$user_id	= $this->session->userdata("user_id");
			$user		= $this->global_model->get_data("mst_user", array(
				"user_id"	=> $user_id
			))->row();

			$role		= $this->global_model->get_data("mst_role", array(
				"id"	=> $user->role_id
			))->row();
			
			if (empty($user)){

				$this->session->sess_destroy();
				redirect(base_url("auth/login"));
			}else{
			    $this->_user    = (object) array_merge((array) $user, (array) $role);
            }
				
		}
	}
}
?>