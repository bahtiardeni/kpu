<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Global_model $global_model
 */

class Access
{
    protected $_ci;

    function __construct()
    {
        $this->_ci =& get_instance();

        $this->_ci->load->model('global_model');
    }

    function is_login()
    {
        if($this->_ci->session->userdata("user_id"))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function login($username = "", $password = "")
    {
        $result		= "error";
        $message	= "";

        if (empty($username) && empty($password))
        {
            $message	= "Email atau Password tidak boleh kosong.";
        }
        else
        {
            $user_username	= $this->_ci->global_model->get_data("mst_user", array(
                "username"	=> $username,
                /*"password"	=> md5($password),*/
                "status" => 1
            ))->row();

            $user_email	= $this->_ci->global_model->get_data("mst_user", array(
                "email"		=> $username,
                /*"password"	=> md5($password),*/
                "status" => 1
            ))->row();

            $check_password = false;

            if (!empty($user_username)){

                $check_password = true;
            }else if (!empty($user_email)){

                $check_password = true;
            }else{
                $message	= "Username yang Anda masukan salah.";
            }

            $log_login	= array();

            if ($check_password){
                if (!empty($user_username)){
                    $user			= $user_username;
                    $field_username	= "username";
                }else{
                    $user			= $user_email;
                    $field_username	= "email";
                }


                // Check Password
                if ($user->password == md5(trim($password)))
                {
                    $result	= "success";

                    $this->_ci->session->set_userdata(array(
                        "user_id"	=> $user->user_id
                    ));
                }
                else
                {
                    $message	= "Password yang Anda memasukan salah.";
                }
            }
        }

        return array(
            "result"	=> $result,
            "message"	=> $message
        );
    }
}

?>