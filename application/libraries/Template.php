<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template 
{
	protected $_ci;
	
	protected $_js;
	protected $_css;
	protected $_user;
	
	function __construct() 
	{
		$this->_ci =& get_instance();
		
		$this->_css	= array(
			"public/homer/vendor/fontawesome/css/font-awesome.css",
		    "public/homer/vendor/metisMenu/dist/metisMenu.css",
		    "public/homer/vendor/animate.css/animate.css",
		    "public/homer/vendor/bootstrap/dist/css/bootstrap.css",
		    "public/homer/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css",
		    "public/homer/fonts/pe-icon-7-stroke/css/helper.css",
		    "public/homer/vendor/toastr/build/toastr.min.css",
		    "public/homer/vendor/sweetalert/lib/sweet-alert.css",
		    "public/homer/styles/style.css",
		    "public/css/style.css",
		);
		
		$this->_js	= array(
			"public/homer/vendor/jquery/dist/jquery.min.js",
			"public/homer/vendor/slimScroll/jquery.slimscroll.min.js",
			"public/homer/vendor/bootstrap/dist/js/bootstrap.min.js",
			"public/homer/vendor/metisMenu/dist/metisMenu.min.js",
			"public/homer/vendor/iCheck/icheck.min.js",
			"public/homer/vendor/sparkline/index.js",
			"public/homer/vendor/toastr/build/toastr.min.js",
			"public/homer/vendor/sweetalert/lib/sweet-alert.min.js",
			"public/homer/scripts/homer.js",
			"public/js/script.js",

		);
	}
	
	function generate_template($content, $data = NULL)
	{
		$this->_user			= $data["_user"];
		$data["_css"]			= $this->generate_css($data);
		$data["_js"]			= $this->generate_js($data);
		
		$data["_header"]	    = $this->generate_header($data);
		$data["_navigation"]	= $this->generate_navigation($data);

		$data["_content"]		= $this->_ci->load->view($content, $data, TRUE);
		
		
		$this->_ci->load->view("template/template", $data);
	}
	
	protected function generate_css($data)
	{
		$content	= "";
		
		if (!empty($data["css"])){
			$this->_css	= array_merge($this->_css, $data["css"]);
		}
		
		foreach ($this->_css as $css)
		{
			$content .= "<link rel='stylesheet' media='screen'  href='".base_url($css."?v=".time())."'>";
		}
		
		return $content;
	}
	
	protected function generate_js($data)
	{
		$content	= "";
		
		if (!empty($data["js"])){
			$this->_js	= array_merge($this->_js, $data["js"]);
		}
		
		foreach ($this->_js as $js)
		{
			$content .= "<script src='".base_url($js."?v=".time())."'></script>";
		}
		
		return $content;
	}

    protected function generate_header($data)
    {
        return $this->_ci->load->view("template/header", $data, true);
    }
	
	protected function generate_navigation($data)
	{
		return $this->_ci->load->view("template/navigation", $data, true);
	}
	
	protected function generate_sidebar($data)
	{	
		return $this->_ci->load->view("template/sidebar", $data, true);
	}
	
}

?>