<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model 
{
	
	function __construct() 
	{
		parent::__construct();
	}
	
	function get_data($condition = array(), $order_field = "", $order_type = "ASC", $page = null, $data_size = null)
	{
		if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
		$data	= $this->db;
		$data->select("mst_user.*");
		$data->select("mst_role.role_name as role_name");
		$data->from("mst_user");
		$data->JOIN("mst_role", "mst_user.role_id = mst_role.id", "left");
		
		foreach ($condition as $field => $value)
		{
            $data->like($field, $value);
			/*$data->where($field, $value);*/
		}

        $data->where("role_id !=", 1);
		
		if (!empty($order_field) && !empty($order_type)){
			$data->order_by($order_field, $order_type);
		}
		
		if ((!empty($page) || $page == 0 ) && !empty($data_size)){
			$data->limit((int)$data_size, $page);
		}
		
		$get	= $data->get();
		
		return $get;
	}
	
	
}

?>