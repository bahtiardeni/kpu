<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Aplikasi Perhitungan Suara</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/vendor/toastr/build/toastr.min.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?=base_url("public/homer")?>/styles/style.css">
    <link rel="stylesheet" href="<?=base_url("public/css")?>/style.css">

</head>
<body class="blank" id="page-login">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Homer - Responsive Admin Theme</h1><p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>SILAHKAN LOGIN</h3>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <form action="#" method="post" id="loginForm">
                        <div class="form-group">
                            <label class="control-label" for="username">Username</label>
                            <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            <span class="help-block small">Masukan username</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">Password</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                            <span class="help-block small">Masukan password</span>
                        </div>
                        <button type="button" class="btn btn-primary btn-block btn-login">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Vendor scripts -->
<script src="<?=base_url("public/homer")?>/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/iCheck/icheck.min.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/sparkline/index.js"></script>
<script src="<?=base_url("public/homer")?>/vendor/toastr/build/toastr.min.js"></script>

<script>
    var base_url = "<?=base_url()?>";
</script>

<!-- App scripts -->
<script src="<?=base_url("public/homer")?>/scripts/homer.js"></script>
<script src="<?=base_url("public/js")?>/script.js"></script>

</body>
</html>