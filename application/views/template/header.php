<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
        <span>
            PILKADA
        </span>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="small-logo">
            <span class="text-primary">PILKADA</span>
        </div>
        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="" href="<?=base_url("user/profile")?>">Profil</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url("auth/logout")?>">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav no-borders">
                <li>
                    <a href="<?=base_url("user/profile")?>" title="Profil">
                        <i class="pe-7s-user pe-7s-news-paper"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?=base_url("auth/logout")?>" title="Logout">
                        <i class="fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
