<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- Navigation -->
<aside id="menu">
    <div id="navigation">

        <ul class="nav" id="side-menu">

            <!--Menu Administrator-->
            <?php if ($_user->role_id == 1){ ?>

                <li>
                    <a href="<?=base_url()?>"><span class="fa fa-tv"></span> <span class="nav-label">Dashboard</span> </a>
                </li>
                <li>
                    <a href="<?=base_url()?>"><span class="fa fa-list-alt"></span> <span class="nav-label">Data Master</span> </a>
                </li>

                <li>
                    <a href="#" aria-expanded="true"><span class="fa fa-list-alt"></span> <span class="nav-label">Box transitions</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level collapse " aria-expanded="true" style="">
                        <li><a href="overview.html"><span class="label label-success pull-right">Start</span> Overview </a>  </li>
                        <li><a href="transition_two.html">Columns from up</a></li>
                        <li><a href="transition_one.html">Columns custom</a></li>
                        <li><a href="transition_three.html">Panels zoom</a></li>
                        <li><a href="transition_four.html">Rows from down</a></li>
                        <li><a href="transition_five.html">Rows from right</a></li>
                    </ul>
                </li>

            <!--Menu Admin-->
            <?php } else if ($_user->role_id == 2){ ?>

                <li>
                    <a href="<?=base_url()?>"><span class="fa fa-tv"></span> <span class="nav-label">Dashboard</span> </a>
                </li>
                <li>
                    <a href="#" aria-expanded="true"><span class="fa fa-list-alt"></span> <span class="nav-label">Data Master</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level collapse " aria-expanded="true" style="">
                        <li><a href="<?=base_url("user")?>">Data User</a></li>
                        <li><a href="<?=base_url("tps")?>">Data TPS</a></li>
                    </ul>
                </li>

            <?php } ?>


        </ul>
    </div>
</aside>
