<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $title ?></title>
    <?php echo $_css; ?>

</head>
<body class="fixed-navbar sidebar-scroll">

<!-- Simple splash screen-->
<div class="splash">
    <div class="color-line"></div>
    <div class="splash-title">
        <h1>Homer - Responsive Admin Theme</h1>
        <p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<?=$_header; ?>
<?=$_navigation; ?>
<!-- Main Wrapper -->
<div id="wrapper">

    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">
                    <?=$title;?>
                </h2>
                <small>Sistem Perhitungan Suara dan Kinerja Tim </small>
            </div>
        </div>
    </div>

    <?=$_content;?>
    <!-- Footer-->
    <br>
    <footer class="footer">
        <span class="pull-right">
            <a href="mailto:bahtiardeni@gmail.com" target="_top">bahtiardeni@gmail.com</a>
        </span>
        Copyright &copy; <?=date("Y")?>
    </footer>

</div>

<script>
    var base_url	= "<?=base_url()?>";
</script>
<?php echo $_js; ?>

</body>
</html>
