<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">

                    <form id="form_datatable" method="post">

                        <div class="row">
                            <div class="col-lg-12">

                                <div class="btn-group">
                                    <a href="<?=base_url($link_update)?>" class="btn btn-sm btn-primary btn-data-add">Tambah Data</a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" readonly name="type" id="type" value="<?=$datatable?>" />
                        <input type="hidden" readonly name="link_update" value="<?=$link_update?>" />

                        <div class="row">
                            <div class="col-lg-12">

                                <div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="th-input">
                                                    <!--<input type="text" name="search[role_id]" class="form-control input-sm" placeholder="Nama Klasifikasi"/>-->

                                                    <select name="search[role_id]" class="form-control input-sm">
                                                        <option value="-all-">Pilih</option>
                                                        <?php foreach ($roles as $role) { ?>
                                                            <option value="<?=$role->id?>"><?=$role->role_name?></option>
                                                        <?php } ?>

                                                    </select>

                                                </th>
                                                <th class="th-input"><input type="text" name="search[name]" class="form-control input-sm" placeholder="Nama"/></th>
                                                <th class="th-input hidden-xs"><input type="text" name="search[username]" class="form-control input-sm" placeholder="Username"/></th>
                                                <th class="th-input hidden-xs"><input type="text" name="search[email]" class="form-control input-sm" placeholder="Email"/></th>
                                                <th class="th-input hidden-xs"><input type="text" name="search[hp]" class="form-control input-sm" placeholder="HP"/></th>
                                                <th class="th-input hidden-xs"><input type="text" name="search[created]" class="form-control input-sm" placeholder="Created"/></th>
                                                <th class="th-input hidden-xs"><input type="text" name="search[updated]" class="form-control input-sm" placeholder="Updated"/></th>
                                                <th class="th-input" style="min-width: 70px"></th>
                                            </tr>
                                            <tr>
                                                <th>Role</th>
                                                <th>Nama</th>
                                                <th class="hidden-xs">Username</th>
                                                <th class="hidden-xs">Email</th>
                                                <th class="hidden-xs">HP</th>
                                                <th class="hidden-xs">Created</th>
                                                <th class="hidden-xs">Updated</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-xs-4">
                                <select name="size" id="size" aria-controls="data_table" class="form-control input-sm">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                            <div class="col-lg-4  col-md-4 col-xs-8 pull-right text-right">
                                <div id="pagination"></div>
                                <input type="hidden" id="page" name="page" value="1">
                            </div>
                            <div class="col-lg-6  col-xs-12" style="padding-top: 5px">
                                <span id="text_count"></span>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

</div>

