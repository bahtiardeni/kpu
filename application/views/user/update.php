<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">

					<div id="notification-error"></div>
					<form id="form_update_datatable" method="post">
						<?=$type?>
						<?=$input["user_id"]?>
						<div class="form-group">
							<label>Role <span class="required">*</span></label>
							<?=$input["role_id"]?>
						</div>
						<div class="form-group">
							<label>Username <span class="required">*</span></label>
							<?=$input["username"]?>
						</div>
                        <div class="form-group">
                            <label>Email <span class="required">*</span></label>
                            <?=$input["email"]?>
                        </div>
						<div class="form-group">
							<label>Password <span class="required">*</span></label>
							<?=$input["password"]?>
							<?=$input["old_password"]?>
						</div>
						<div class="form-group">
							<label>Nama Lengkap <span class="required">*</span></label>
							<?=$input["name"]?>
						</div>
						<div class="form-group">
							<label>HP</label>
							<?=$input["hp"]?>
						</div>
						<div class="form-group">
							<label>Status <span class="required">*</span></label>
							<?=$input["status"]?>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-sm btn-primary btn-save">Simpan</button>
							<a href="<?=$link_back?>" class="btn btn-sm btn-danger btn-cancel ">Batal</a>
						</div>
					</form>

                </div>
            </div>
        </div>
    </div>
</div>