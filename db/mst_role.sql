/*
Navicat MySQL Data Transfer

Source Server         : localaing
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : kpu

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-03-08 16:23:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mst_role
-- ----------------------------
DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_role
-- ----------------------------
INSERT INTO `mst_role` VALUES ('1', 'Administrator', '2018-03-07 22:20:01', '2018-03-07 22:20:01');
INSERT INTO `mst_role` VALUES ('2', 'Admin', '2018-03-07 22:20:01', '2018-03-07 22:20:21');
INSERT INTO `mst_role` VALUES ('3', 'Reporting', '2018-03-07 22:20:01', '2018-03-07 22:20:01');
INSERT INTO `mst_role` VALUES ('4', 'Team', '2018-03-07 22:20:01', '2018-03-07 22:20:01');

-- ----------------------------
-- Table structure for mst_user
-- ----------------------------
DROP TABLE IF EXISTS `mst_user`;
CREATE TABLE `mst_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_role` (`role_id`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_user
-- ----------------------------
INSERT INTO `mst_user` VALUES ('1', 'administrator', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081514568878', '', '1', '2018-03-07 16:38:20', '2018-03-07 23:11:50');
INSERT INTO `mst_user` VALUES ('2', 'admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '2', '2018-03-07 16:38:20', '2018-03-07 23:11:48');
INSERT INTO `mst_user` VALUES ('3', 'reporting', 'Reporting', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '3', '2018-03-07 16:38:20', '2018-03-08 14:26:39');
INSERT INTO `mst_user` VALUES ('4', 'team1', 'Team 1', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:26:59', '2018-03-08 14:32:00');
INSERT INTO `mst_user` VALUES ('5', 'user1', 'User 1', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('6', 'user2', 'User 2', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('7', 'user3', 'User 3', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('8', 'user4', 'User 4', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('9', 'user5', 'User 5', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('10', 'user6', 'User 6', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('11', 'user7', 'User 7', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('12', 'user8', 'User 8', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('13', 'user9', 'User 9', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('14', 'user10', 'User 10', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('15', 'user11', 'User 11', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('16', 'user12', 'User 12', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('17', 'user13', 'User 13', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('18', 'user14', 'User 14', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('19', 'user15', 'User 15', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('20', 'user16', 'User 16', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
INSERT INTO `mst_user` VALUES ('21', 'user17', 'User 17', '21232f297a57a5a743894a0e4a801fc3', 'bahtiardeni@gmail.com', '081222644490', '', '4', '2018-03-08 14:31:34', '2018-03-08 14:31:34');
