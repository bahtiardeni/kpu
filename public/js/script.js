var jScript = function () {
    var ajaxDataTable;
    var image_loading 	= "<center class='image-loading'><img src='" + base_url + "public/images/loading.gif' style='width: 32px; margin: 20px;' /></center>";

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var handlePageLogin = function () {
        if ($("#page-login").length)
        {
            var pageEl = $("#page-login");

            // Handle Login
            pageEl.on('click', '.btn-login', function (e) {

                $.ajax({
                    url: base_url + "ajax/do_login",
                    type: 'POST',
                    dataType : "JSON",
                    data: pageEl.find("form").serialize(),
                    success: function (data) {
                        if (data.result == "error"){
                            toastr.error(data.message);
                        }else if (data.result == "success"){
                            location.href = base_url;
                        }
                    }
                });

            });


        }
    }

    // Datatable
    var handleDataTable = function(){
        var element	= $("#form_datatable");

        if (element.length)
        {
            var colspan = element.find( "table thead tr:nth-child(1) th" ).length ;

            if(ajaxDataTable) ajaxDataTable.abort();
            ajaxDataTable = $.ajax({
                type: "GET",
                dataType : "JSON",
                url: base_url + 'ajax/datatable/',
                beforeSend : function(){
                    element.find("table tbody").html("<tr><td colspan='" + colspan + "' style='background-color: #fff'>" + image_loading + "</td></tr>");
                },
                data : element.serialize(),
                success: function (data) {

                    var table_content	= "";

                    $.each(data.datatable, function(key, row_data) {

                        table_content += "<tr>";

                        $.each(row_data, function(key_row, row) {

                            var td_class    = "";

                            if(jQuery.inArray(key_row, data.field_mobile) === -1){
                                td_class += " hidden-xs";
                            }

                            if (key_row == "action"){
                                td_class += " text-center";
                            }

                            table_content += "<td class='" + td_class + "'>" + row + "</td>";
                        });

                        table_content += "</tr>";

                    });

                    if (data.datatable.length == 0)
                    {
                        table_content	= "<tr><td colspan='" + colspan + "' style='background-color: #fff; text-align: center; color: #ff0000; padding: 10px;'><b>Data tidak ditemukan.</b></td></tr>";
                    }

                    if ($("#type").val() == "realisasi"){

                        handleClearNotification();

                        if ($("#eselon_id").val() == "" || $("#satker_id").val() == "")
                        {
                            table_content	= "<tr><td colspan='" + colspan + "' style='background-color: #fff; text-align: center; color: #ff0000; padding: 10px;'><b>Silahkan pilih dulu Eselon dan Satuan Kerja.</b></td></tr>";

                            element.find("table tbody").html(table_content);

                            element.find(".btn-save").addClass("hidden");
                        }
                        else
                        {
                            element.find("table tbody").html(table_content);
                            element.find("#text_count").text("Total data : " + data.count);

                            $(".number").number(true, 0);
                            $(".number").addClass("text-right");
                            $(".number").attr("onfocus", "this.select();");

                            handleTotalRealisasi();
                            element.find(".btn-save").removeClass("hidden");
                        }
                    }
                    else
                    {
                        element.find("table tbody").html(table_content);
                        element.find("#text_count").text("Total data : " + data.count);
                    }

                    handlePagnation(element, parseInt(element.find("#page").val()), parseInt($("#size").val()), data.count);
                    handleFilterDataTable();
                },
                error: function (jqXHR, exception) {

                },
            });
        }
    }

    var handlePagnation	= function(element, paged, size, count){

        var total_page  = parseInt(count/size);
        var content	    = "";

        if (total_page >= 1)
        {
            content	+= "<ul class='pagination'>";
            content	+= "<li class='paginate_button previous " + (paged == 1 ? "disabled" : "") + " id='example1_previous' data-page='" + (paged - 1) + "'><a href='javascript:void(0)' > Prev </a></li>";
            //content	+= "<li class=' active'><a href='javascript:void(0)'>" + paged + "</a></li>";
            content	+= "<li class='paginate_button next " + (paged == total_page ? "disabled" : "") + "' id='example1_next' data-page='" + (paged + 1) + "'><a href='javascript:void(0)' > Next </a></li>";
            content	+= "</ul>";
        }

        element.find("#pagination").html(content);
    }

    var handleFilterDataTable = function(){
        var element_id	= "#form_datatable";

        $(document).on("click", ".btn-refresh", function (e) {
            handleDataTable();
        });

        $(document).on("keyup", element_id + " input", function (e) {

            if (!$(this).hasClass("textbox-realisasi")){
                $("#page").val(1);
                handleDataTable();
            }

        });

        $(document).on("change", element_id + " select", function (e) {

            if ($(this).attr("id") != "eselon_id")
            {
                $("#page").val(1);
                handleDataTable();
            }
            else
            {
                var eselon_id = $(this).val();

                //alert(eselon_id);

                $.ajax({
                    url: base_url + "ajax/get_satker",
                    dataType : "json",
                    type: "POST",
                    data: {
                        eselon_id : eselon_id
                    },
                    beforeSend : function(){
                        $("#satker_id").html("<option>Silahkan tunggu...</option>");
                        $("#satker_id").attr("disabled", true);
                    },
                    success: function (data) {

                        var content	= "<option value=''>Pilih</option>";

                        $.each(data, function(key, row_data) {
                            content	+= "<option value='"+row_data.satker_id+"'>"+row_data.namasatker+"</option>";
                        });


                        $("#satker_id").html(content);
                        $("#satker_id").attr("disabled", false);

                        $("#page").val(1);
                        handleDataTable();
                    }
                });
            }


        });

        $(document).on("click", element_id + " table thead tr th.sort", function (e) {

            var sort_field	= $(this).data("sort");

            $(element_id + " table thead tr th.sort span").html("");

            if ($(this).hasClass("sort-asc")){
                $(element_id + " table thead tr th.sort").removeClass("sort-asc").removeClass("sort-desc");
                $(this).addClass("sort-desc");
                $(this).find("span").html(sort_desc);
                $(element_id + " #sort_type").val("desc");
            }else{
                $(element_id + " table thead tr th.sort").removeClass("sort-asc").removeClass("sort-desc");
                $(this).addClass("sort-asc");
                $(this).find("span").html(sort_asc);
                $(element_id + " #sort_type").val("asc");
            }

            $(element_id + " #sort_name").val(sort_field);
            $(element_id + " #page").val(1);
            handleDataTable();
        });

        $(document).on("click", element_id + "  .paginate_button", function (e) {

            if (!$(this).hasClass("disabled"))
            {
                $(element_id + " #page").val($(this).data("page"));

                handleDataTable();
            }
        });

        $(document).on('click', '#form_datatable .btn-remove-data', function () {

            handleClearNotification();

            var el = $(this);

            swal({
                title: "Apakah Anda yakin?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya"
            }).then(function () {
                $.ajax({
                    url: base_url + 'ajax/datatable_delete/',
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        type : $("#type").val(),
                        id : el.data("id")
                    },
                    success: function (res) {
                        if (res.result == "success"){
                            handleDataTable();
                        }else{
                            handleNotificationError(res.message);
                        }
                    },
                    error: function (jqXHR, exception) {

                    }
                });
            }, function (dismiss) {

            });
        });

        $(document).on('click', '#form_datatable .btn-reset-password', function () {

            handleClearNotification();

            var el = $(this);

            swal({
                title: "Apakah Anda yakin?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya"
            }).then(function () {
                $.ajax({
                    url: base_url + 'ajax/reset_password/',
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        type : $("#type").val(),
                        id : el.data("id")
                    },
                    success: function (res) {
                        if (res.result == "success"){
                            swal(
                                'Password berhasil direset!',
                                '',
                                'success'
                            )
                        }else{
                            handleNotificationError(res.message);
                        }
                    },
                    error: function (jqXHR, exception) {

                    }
                });
            }, function (dismiss) {

            });
        });
    }

    var handleUpdateDataTable = function(){
        var element	= $("#form_update_datatable");

        if (element.length)
        {
            $(document).on("click", "#form_update_datatable .btn-save", function () {

                swal({
                    title: "Apakan Anda yakin?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "Ya"
                }, function (isConfirm) {
                    if (isConfirm)
                    {
                        $.ajax({
                            url: base_url + "ajax/datatable_update",
                            type: "POST",
                            dataType: "JSON",
                            data: $("#form_update_datatable").serialize(),
                            beforeSend : function(){
                                $("#form_update_datatable input, textarea, select, button").attr("disabled", true);
                                $("#form_update_datatable .btn-save").text("Silahkan tunggu...");
                            },
                            success: function (res) {
                                if (res.result == "success"){
                                    window.location	= $("#form_update_datatable .btn-cancel").attr("href");
                                }else{
                                    toastr.error(res.message);
                                }

                                $("#form_update_datatable input, textarea, select, button").attr("disabled", false);
                                $("#form_update_datatable .btn-save").text("Simpan");
                            },
                            error: function (jqXHR, exception) {
                                $("#form_update_datatable input, textarea, select, button").attr("disabled", false);
                                $("#form_update_datatable .btn-save").text("Simpan");
                            },
                        });

                    }


                });
            });
        }
    }

    return {
        initialize: function () {
            handlePageLogin();
            handleDataTable();
            handleUpdateDataTable()
        }
    };
}();

jQuery(document).ready(function () {
    jScript.initialize();
});